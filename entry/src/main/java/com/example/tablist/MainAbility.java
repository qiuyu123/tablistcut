package com.example.tablist;

import com.example.tablist.fraction.AttentionFraction;
import com.example.tablist.fraction.HotspotFraction;
import com.example.tablist.fraction.QuestionFraction;
import com.example.tablist.fraction.RecommendFraction;
import ohos.aafwk.ability.fraction.FractionAbility;
import ohos.aafwk.content.Intent;
import ohos.agp.components.TabList;



/**
 *
 * 创建人：xuqing
 * 创建时间：2020年12月20日15:33:53
 * 类说明：  tablist 示例
 *
 *
 */


public class MainAbility extends FractionAbility {
    private  String[] str={"关注","推荐","热点","问答"};

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        initview();
    }

    private void initview() {
        TabList tabList = (TabList) findComponentById(ResourceTable.Id_tab_list);
        if(tabList!=null){
            for (int i = 0; i < str.length; i++) {
                TabList.Tab tab = tabList.new Tab(getContext());
                tab.setText(str[i]);
                tabList.addTab(tab);
            }
           /* tabList.setTabLength(60); // 设置Tab的宽度
            tabList.setTabMargin(26); // 设置两个Tab之间的间距*/
            addHomeFraction();
            tabList.addTabSelectedListener(new TabList.TabSelectedListener() {
                @Override
                public void onSelected(TabList.Tab tab) {
                    // 当某个Tab从未选中状态变为选中状态时的回调

                    System.out.println("当某个Tab从未选中状态变为选中状态时的回调");

                   /* new ToastDialog(MainAbility.this)
                            .setText("从未选中状态变为选中状态时的回调")
                            .setAlignment(LayoutAlignment.CENTER)
                            .show();*/

                    layoutShow(tab.getPosition());
                }

                @Override
                public void onUnselected(TabList.Tab tab) {
                    // 当某个Tab从选中状态变为未选中状态时的回调
          /*          new ToastDialog(MainAbility.this)
                            .setText("从选中状态变为未选中状态时的回调")
                            .setAlignment(LayoutAlignment.CENTER)
                            .show();*/
                    System.out.println("当某个Tab从选中状态变为未选中状态时的回调");
                }

                @Override
                public void onReselected(TabList.Tab tab) {
                    // 当某个Tab已处于选中状态，再次被点击时的状态回调
               /*     new ToastDialog(MainAbility.this)
                            .setText("已处于选中状态，再次被点击时的状态回调")
                            .setAlignment(LayoutAlignment.CENTER)
                            .show();*/
                    System.out.println("当某个Tab已处于选中状态，再次被点击时的状态回调");
                }
            });

        }
    }


    private void  addHomeFraction(){
        getFractionManager()
                .startFractionScheduler()
                .add(ResourceTable.Id_mainstack, new AttentionFraction())
                .submit();
    }


    public  void  layoutShow(int  code){
        switch (code){
            case 0:
                getFractionManager()
                        .startFractionScheduler()
                        .replace(ResourceTable.Id_mainstack, new AttentionFraction())
                        .submit();

                break;
            case 1:
                getFractionManager()
                        .startFractionScheduler()
                        .replace(ResourceTable.Id_mainstack, new RecommendFraction())
                        .submit();


                break;
            case 2:
                getFractionManager()
                        .startFractionScheduler()
                        .replace(ResourceTable.Id_mainstack, new HotspotFraction())
                        .submit();

                break;
            case 3:
                getFractionManager()
                        .startFractionScheduler()
                        .replace(ResourceTable.Id_mainstack, new QuestionFraction())
                        .submit();

                break;
            default:
                break;
        }
    }
}
