package com.example.tablist.fraction;

import com.example.tablist.ResourceTable;
import com.example.tablist.bean.CompanyInfo;
import com.example.tablist.config.Api;
import com.example.tablist.provider.CompanyProvicer;
import com.google.gson.Gson;
import ohos.aafwk.ability.fraction.Fraction;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.ListContainer;

import java.util.List;


/***
 *
 * 创建人：xuqing
 * 创建2021年2月28日17:24:03
 * 类说明：推荐模块
 */


public class RecommendFraction extends Fraction {
    private CompanyProvicer  companyProvicer;
    private ListContainer companylistComtainer;

    @Override
    protected Component onComponentAttached(LayoutScatter scatter, ComponentContainer container, Intent intent) {

        Component component=scatter.parse(ResourceTable.Layout_fraction_recommend,container,false);

        return component;
    }

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        companylistComtainer= (ListContainer) getFractionAbility().findComponentById(ResourceTable.Id_company_page_list);
        getCompany();

    }


    public  void  getCompany(){
        Gson gson=new Gson();
        CompanyInfo companyInfo=gson.fromJson(Api.getCompanyInfo(), CompanyInfo.class);
        List<CompanyInfo.DataBean> datalist=companyInfo.getData();
        companyProvicer=new CompanyProvicer(datalist, getFractionAbility());
        companylistComtainer.setItemProvider(companyProvicer);
    }




}
