package com.example.tablist.fraction;

import com.example.tablist.ResourceTable;
import com.example.tablist.bean.MessageInfo;
import com.example.tablist.config.Api;
import com.example.tablist.provider.MessaegsProvicer;
import com.google.gson.Gson;
import ohos.aafwk.ability.fraction.Fraction;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.ListContainer;

import java.util.List;


/***
 *
 * 创建人：xuqing
 * 创建2021年2月28日17:24:03
 * 类说明： 热点模块
 *
 *
 *
 */

public class HotspotFraction extends Fraction {
    private MessaegsProvicer messaegsProvicer;
    private ListContainer messagelistComtainer;

    @Override
    protected Component onComponentAttached(LayoutScatter scatter, ComponentContainer container, Intent intent) {
        Component component=scatter.parse(ResourceTable.Layout_fraction_hotspot,container,false);

        return component;
    }

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);


        messagelistComtainer= (ListContainer) getFractionAbility().findComponentById(ResourceTable.Id_message_page_list);
        getMessage();
    }


    public  void  getMessage(){
        Gson gson=new Gson();
        MessageInfo messageInfo=gson.fromJson(Api.getMessageinfo(),MessageInfo.class);
        List<MessageInfo.DataBean> datalist=messageInfo.getData();
        messaegsProvicer=new MessaegsProvicer(datalist, getFractionAbility());
        messagelistComtainer.setItemProvider(messaegsProvicer);
    }

}
