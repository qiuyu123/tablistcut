package com.example.tablist.fraction;

import com.example.tablist.ResourceTable;
import com.example.tablist.bean.PositionInfo;
import com.example.tablist.config.Api;
import com.example.tablist.provider.PositionProvider;
import com.google.gson.Gson;
import ohos.aafwk.ability.fraction.Fraction;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.ListContainer;

import java.util.List;


/***
 *
 * 创建人：xuqing
 * 创建2021年2月28日17:24:03
 * 类说明： 关注模块
 *
 *
 *
 */

public class AttentionFraction extends Fraction {


    private PositionProvider positionProvider;
    private ListContainer listContainer;
    @Override
    protected Component onComponentAttached(LayoutScatter scatter, ComponentContainer container, Intent intent) {
        Component component=scatter.parse(ResourceTable.Layout_fraction_attention,container,false);
        return component;
    }


    protected void onStart(Intent intent) {
        super.onStart(intent);
        listContainer= (ListContainer) getFractionAbility().findComponentById(ResourceTable.Id_jop_page_list);
        getPostition();
    }
    public  void  getPostition(){

        Gson gson=new Gson();
        PositionInfo positionInfo=gson.fromJson(Api.getPositioninfo(),PositionInfo.class);
        List<PositionInfo.DataBean> list=positionInfo.getData();
        positionProvider=new PositionProvider(list,  getFractionAbility());
        listContainer.setItemProvider(positionProvider);



    }





}
