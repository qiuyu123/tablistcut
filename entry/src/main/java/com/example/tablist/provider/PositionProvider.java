package com.example.tablist.provider;

import com.example.tablist.ResourceTable;
import com.example.tablist.bean.PositionInfo;
import ohos.aafwk.ability.Ability;
import ohos.agp.components.*;
import java.util.List;

public class PositionProvider extends RecycleItemProvider {

    private List<PositionInfo.DataBean> list;
    private Ability slice;
    public PositionProvider(List<PositionInfo.DataBean> list, Ability slice) {
        this.list = list;
        this.slice = slice;
    }
    @Override
    public int getCount() {
        return list.size();
    }
    @Override
    public Object getItem(int position) {
        return list.get(position);
    }
    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public Component getComponent(int i, Component component, ComponentContainer componentContainer) {
        if (component== null) {
            Component     cpt = LayoutScatter.getInstance(slice).parse(ResourceTable.Layout_position_listview_item,null,false);
            PositionInfo.DataBean  dataBean = list.get(i);
            Text position_nametext =(Text) cpt.findComponentById(ResourceTable.Id_position_name);
            Text position_namesizetext =(Text) cpt.findComponentById(ResourceTable.Id_position_namesize);
            Text text_hr =(Text) cpt.findComponentById(ResourceTable.Id_text_hr);
            Text position_salary_text =(Text) cpt.findComponentById(ResourceTable.Id_position_salary);
            position_nametext.setText(dataBean.getName());
            position_namesizetext.setText(dataBean.getCname()+dataBean.getSize());
            text_hr.setText(dataBean.getUsername()+"|"+dataBean.getTitle());
            position_salary_text.setText(dataBean.getSalary());
            return cpt;
        } else {
            return component;
        }
    }






}
