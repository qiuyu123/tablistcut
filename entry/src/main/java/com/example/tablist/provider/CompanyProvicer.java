package com.example.tablist.provider;

import com.example.tablist.ResourceTable;
import com.example.tablist.bean.CompanyInfo;
import ohos.aafwk.ability.Ability;
import ohos.agp.components.*;
import java.util.List;

public class CompanyProvicer extends RecycleItemProvider {

    private List<CompanyInfo.DataBean> list;
    private Ability slice;
    public CompanyProvicer(List<CompanyInfo.DataBean> list, Ability slice) {
        this.list = list;
        this.slice = slice;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public Component getComponent(int i, Component component, ComponentContainer componentContainer) {
        if (component== null) {
            Component     cpt = LayoutScatter.getInstance(slice).parse(ResourceTable.Layout_company_listview_item,null,false);
            CompanyInfo.DataBean  dataBean = list.get(i);
            Text company_nametext =(Text) cpt.findComponentById(ResourceTable.Id_company_nametext);
            Text type_size_text =(Text) cpt.findComponentById(ResourceTable.Id_type_size_text);
            Text location_text =(Text) cpt.findComponentById(ResourceTable.Id_location_text);
            Text company_hot_text =(Text) cpt.findComponentById(ResourceTable.Id_company_hot_text);
            company_nametext.setText(dataBean.getName());
            type_size_text.setText(dataBean.getType()+"|"+dataBean.getSize()+"|"+dataBean.getEmployee());
            location_text.setText(dataBean.getLocation());
            company_hot_text.setText("热招"+dataBean.getHot()+"等"+dataBean.getCount()+"个职位");

            return cpt;
        } else {
            return component;
        }
    }







}
