package com.example.tablist.provider;
import com.example.tablist.ResourceTable;
import com.example.tablist.bean.MessageInfo;
import ohos.aafwk.ability.Ability;
import ohos.agp.components.*;
import java.util.List;





public class MessaegsProvicer extends RecycleItemProvider {

    private List<MessageInfo.DataBean> list;
    private Ability slice;
    public MessaegsProvicer(List<MessageInfo.DataBean> list, Ability slice) {
        this.list = list;
        this.slice = slice;
    }


    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public Component getComponent(int i, Component component, ComponentContainer componentContainer) {
        if (component== null) {
            Component     cpt = LayoutScatter.getInstance(slice).parse(ResourceTable.Layout_message_listview_item,null,false);
            MessageInfo.DataBean  dataBean = list.get(i);
            Text nicknametext =(Text) cpt.findComponentById(ResourceTable.Id_message_nicknametext);
            Text companynametext =(Text) cpt.findComponentById(ResourceTable.Id_message_companynametext);
            Text msg_text =(Text) cpt.findComponentById(ResourceTable.Id_msg_text);
            Image iconimage= (Image) cpt.findComponentById(ResourceTable.Id_message_iconimage);
            System.out.println("url --- > "+dataBean.getHeadportraiturl());
        /*    new Thread(new Runnable() {
                @Override
                public void run() {

                    ImageSource imageSource = ImageSource.create(dataBean.getHeadportraiturl(), new ImageSource.SourceOptions());
                    PixelMap pixelMap = imageSource.createPixelmap(new ImageSource.DecodingOptions());

                    slice.getUITaskDispatcher().asyncDispatch(() -> {
                        //更新UI的操作
                        iconimage.setPixelMap(pixelMap);
                    });

                }
            }).start();*/



            nicknametext.setText(dataBean.getNickname());
             companynametext.setText(dataBean.getCompanyname()+"|"+dataBean.getJobname());
             msg_text.setText(dataBean.getMsg());
            return cpt;
        } else {
            return component;
        }
    }
}
