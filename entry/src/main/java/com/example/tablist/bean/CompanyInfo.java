package com.example.tablist.bean;

import java.util.List;

public class CompanyInfo {


    /**
     * msg : 获取数据成功
     * code : 200
     * data : [{"id":1,"logo":"https://www.itying.com/images/flutter/1.png","name":"杭州蚂蚁金服信息技术有限公司  ","location":"上海新浦东区","type":"互联网","size":"B论","employee":"1000人以上","hot":"资深开放产品技术工程师","count":"500","inc":"蚂蚁金融服务集团(以下称\"蚂蚁金服\")起步于2004年成立的支付宝2014年10月"},{"id":2,"logo":"https://www.itying.com/images/flutter/2.png","name":"百度信息技术有限公司  ","location":"广州天河区","type":"互联网","size":"C论","employee":"500人以上","hot":"全栈工程师","count":"1000","inc":"蚂蚁金融服务集团(以下称\"蚂蚁金服\")起步于2004年成立的支付宝2014年10月"},{"id":3,"logo":"https://www.itying.com/images/flutter/3.png","name":"腾讯科有限公司  ","location":"深圳南山区","type":"互联网","size":"D论","employee":"200人以上","hot":"数据挖掘工程师","count":"200","inc":"蚂蚁金融服务集团(以下称\"蚂蚁金服\")起步于2004年成立的支付宝2014年10月"},{"id":4,"logo":"https://www.itying.com/images/flutter/4.png","name":"字节跳动科技有限公司","location":"北京海淀区","type":"互联网","size":"D论","employee":"1500人以上","hot":"资深架构师","count":"1500","inc":"蚂蚁金融服务集团(以下称\"蚂蚁金服\")起步于2004年成立的支付宝2014年10月"}]
     */

    private String msg;
    private int code;
    private List<DataBean> data;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : 1
         * logo : https://www.itying.com/images/flutter/1.png
         * name : 杭州蚂蚁金服信息技术有限公司
         * location : 上海新浦东区
         * type : 互联网
         * size : B论
         * employee : 1000人以上
         * hot : 资深开放产品技术工程师
         * count : 500
         * inc : 蚂蚁金融服务集团(以下称"蚂蚁金服")起步于2004年成立的支付宝2014年10月
         */

        private int id;
        private String logo;
        private String name;
        private String location;
        private String type;
        private String size;
        private String employee;
        private String hot;
        private String count;
        private String inc;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getLogo() {
            return logo;
        }

        public void setLogo(String logo) {
            this.logo = logo;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getLocation() {
            return location;
        }

        public void setLocation(String location) {
            this.location = location;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getSize() {
            return size;
        }

        public void setSize(String size) {
            this.size = size;
        }

        public String getEmployee() {
            return employee;
        }

        public void setEmployee(String employee) {
            this.employee = employee;
        }

        public String getHot() {
            return hot;
        }

        public void setHot(String hot) {
            this.hot = hot;
        }

        public String getCount() {
            return count;
        }

        public void setCount(String count) {
            this.count = count;
        }

        public String getInc() {
            return inc;
        }

        public void setInc(String inc) {
            this.inc = inc;
        }
    }
}
