package com.example.tablist.bean;
import java.util.List;


/**
 *
 *
 *   创建人：xuqing
 *   创建时间：2021年1月16日16:47:54
 *   类说明：职位信息
 *
 *
 */


public class PositionInfo {


    /**
     * msg : 获取数据成功
     * code : 200
     * data : [{"id":4,"name":"资深安卓工程师","cname":"今日头条","size":"D轮","salary":"40K-60K","username":"Kimi","title":"HR"},{"id":5,"name":"资深安卓工程师","cname":"今日头条","size":"D轮","salary":"40K-60K","username":"Kimi","title":"HR"},{"id":6,"name":"资深安卓工程师","cname":"今日头条","size":"D轮","salary":"40K-60K","username":"Kimi","title":"HR"},{"id":7,"name":"资深安卓工程师","cname":"今日头条","size":"D轮","salary":"40K-60K","username":"Kimi","title":"HR"},{"id":8,"name":"资深安卓工程师","cname":"今日头条","size":"D轮","salary":"40K-60K","username":"Kimi","title":"HR"},{"id":9,"name":"资深安卓工程师","cname":"今日头条","size":"D轮","salary":"40K-60K","username":"Kimi","title":"HR"},{"id":10,"name":"资深安卓工程师","cname":"今日头条","size":"D轮","salary":"40K-60K","username":"Kimi","title":"HR"},{"id":11,"name":"资深安卓工程师","cname":"今日头条","size":"D轮","salary":"40K-60K","username":"Kimi","title":"HR"},{"id":12,"name":"资深安卓工程师","cname":"今日头条","size":"D轮","salary":"40K-60K","username":"Kimi","title":"HR"},{"id":13,"name":"移动端架构师","cname":"银汉游戏","size":"B轮","salary":"15K-20K","username":"刘丽","title":"人事主管"},{"id":14,"name":"Java工程师","cname":"37互娱","size":"D轮","salary":"25K-30K","username":"Reiki","title":"HR-M"}]
     */

    private String msg;
    private int code;
    private List<DataBean> data;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : 4
         * name : 资深安卓工程师
         * cname : 今日头条
         * size : D轮
         * salary : 40K-60K
         * username : Kimi
         * title : HR
         */

        private int id;
        private String name;
        private String cname;
        private String size;
        private String salary;
        private String username;
        private String title;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getCname() {
            return cname;
        }

        public void setCname(String cname) {
            this.cname = cname;
        }

        public String getSize() {
            return size;
        }

        public void setSize(String size) {
            this.size = size;
        }

        public String getSalary() {
            return salary;
        }

        public void setSalary(String salary) {
            this.salary = salary;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }
    }
}
