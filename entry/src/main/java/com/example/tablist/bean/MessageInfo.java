package com.example.tablist.bean;
import java.util.List;

public class MessageInfo {


    /**
     * msg : 获取数据成功
     * code : 200
     * data : [{"id":1,"msg":"你好","jobname":"HR","companyname":"百度","nickname":"小可爱","headportraiturl":"https://www.itying.com/images/flutter/1.png"},{"id":2,"msg":"在吗","jobname":"人事主管","companyname":"腾讯","nickname":"漩涡鸣人","headportraiturl":"https://www.itying.com/images/flutter/2.png"},{"id":3,"msg":"最近在找工作吗","jobname":"人事经理","companyname":"字节跳动","nickname":"雨落之后","headportraiturl":"https://www.itying.com/images/flutter/3.png"},{"id":4,"msg":"你好 在吗？","jobname":"人事助理","companyname":"37互娱","nickname":"马小玲","headportraiturl":"https://www.itying.com/images/flutter/4.png"}]
     */

    private String msg;
    private int code;
    private List<DataBean> data;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : 1
         * msg : 你好
         * jobname : HR
         * companyname : 百度
         * nickname : 小可爱
         * headportraiturl : https://www.itying.com/images/flutter/1.png
         */

        private int id;
        private String msg;
        private String jobname;
        private String companyname;
        private String nickname;
        private String headportraiturl;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getMsg() {
            return msg;
        }

        public void setMsg(String msg) {
            this.msg = msg;
        }

        public String getJobname() {
            return jobname;
        }

        public void setJobname(String jobname) {
            this.jobname = jobname;
        }

        public String getCompanyname() {
            return companyname;
        }

        public void setCompanyname(String companyname) {
            this.companyname = companyname;
        }

        public String getNickname() {
            return nickname;
        }

        public void setNickname(String nickname) {
            this.nickname = nickname;
        }

        public String getHeadportraiturl() {
            return headportraiturl;
        }

        public void setHeadportraiturl(String headportraiturl) {
            this.headportraiturl = headportraiturl;
        }
    }
}
