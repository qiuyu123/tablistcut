package com.example.tablist.config;

public class Api {

    public  static final String BASE_URL ="http://192.168.1.105:8090/";
            //"http://192.168.10.56:8090/";




    //192.168.10.56
    //"http://192.168.1.14:8090/";
    public static final String POSITIONINFO = BASE_URL + "boss/position/getpositioninfo";

    public static final String  MESSAGEINFO=BASE_URL+"boss/message/getmessageinfo";

    public  static final String  COMPANY_INFO=BASE_URL+"boss/company/getcompanyinfo";


    public static final String  COMPAN_DETAILS=BASE_URL+"boss/company/getcompanydetails";


    public  static final String  ADD_POSITION_INFO=BASE_URL+"boss/position/addpositioninfo";

    public  static final String GET_POSITION_DEATAILS=BASE_URL+"boss/position/getpositiondetails";

    public  static  String getPositioninfo(){
        String str="{\n" +
                "    \"msg\": \"获取数据成功\",\n" +
                "    \"code\": 200,\n" +
                "    \"data\": [\n" +
                "        {\n" +
                "            \"id\": 4,\n" +
                "            \"name\": \"资深安卓工程师\",\n" +
                "            \"cname\": \"今日头条\",\n" +
                "            \"size\": \"D轮\",\n" +
                "            \"salary\": \"40K-60K\",\n" +
                "            \"username\": \"Kimi\",\n" +
                "            \"title\": \"HR\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"id\": 5,\n" +
                "            \"name\": \"资深安卓工程师\",\n" +
                "            \"cname\": \"今日头条\",\n" +
                "            \"size\": \"D轮\",\n" +
                "            \"salary\": \"40K-60K\",\n" +
                "            \"username\": \"Kimi\",\n" +
                "            \"title\": \"HR\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"id\": 6,\n" +
                "            \"name\": \"资深安卓工程师\",\n" +
                "            \"cname\": \"今日头条\",\n" +
                "            \"size\": \"D轮\",\n" +
                "            \"salary\": \"40K-60K\",\n" +
                "            \"username\": \"Kimi\",\n" +
                "            \"title\": \"HR\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"id\": 7,\n" +
                "            \"name\": \"资深安卓工程师\",\n" +
                "            \"cname\": \"今日头条\",\n" +
                "            \"size\": \"D轮\",\n" +
                "            \"salary\": \"40K-60K\",\n" +
                "            \"username\": \"Kimi\",\n" +
                "            \"title\": \"HR\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"id\": 8,\n" +
                "            \"name\": \"资深安卓工程师\",\n" +
                "            \"cname\": \"今日头条\",\n" +
                "            \"size\": \"D轮\",\n" +
                "            \"salary\": \"40K-60K\",\n" +
                "            \"username\": \"Kimi\",\n" +
                "            \"title\": \"HR\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"id\": 9,\n" +
                "            \"name\": \"资深安卓工程师\",\n" +
                "            \"cname\": \"今日头条\",\n" +
                "            \"size\": \"D轮\",\n" +
                "            \"salary\": \"40K-60K\",\n" +
                "            \"username\": \"Kimi\",\n" +
                "            \"title\": \"HR\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"id\": 10,\n" +
                "            \"name\": \"资深安卓工程师\",\n" +
                "            \"cname\": \"今日头条\",\n" +
                "            \"size\": \"D轮\",\n" +
                "            \"salary\": \"40K-60K\",\n" +
                "            \"username\": \"Kimi\",\n" +
                "            \"title\": \"HR\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"id\": 11,\n" +
                "            \"name\": \"资深安卓工程师\",\n" +
                "            \"cname\": \"今日头条\",\n" +
                "            \"size\": \"D轮\",\n" +
                "            \"salary\": \"40K-60K\",\n" +
                "            \"username\": \"Kimi\",\n" +
                "            \"title\": \"HR\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"id\": 12,\n" +
                "            \"name\": \"资深安卓工程师\",\n" +
                "            \"cname\": \"今日头条\",\n" +
                "            \"size\": \"D轮\",\n" +
                "            \"salary\": \"40K-60K\",\n" +
                "            \"username\": \"Kimi\",\n" +
                "            \"title\": \"HR\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"id\": 13,\n" +
                "            \"name\": \"移动端架构师\",\n" +
                "            \"cname\": \"银汉游戏\",\n" +
                "            \"size\": \"B轮\",\n" +
                "            \"salary\": \"15K-20K\",\n" +
                "            \"username\": \"刘丽\",\n" +
                "            \"title\": \"人事主管\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"id\": 14,\n" +
                "            \"name\": \"Java工程师\",\n" +
                "            \"cname\": \"37互娱\",\n" +
                "            \"size\": \"D轮\",\n" +
                "            \"salary\": \"25K-30K\",\n" +
                "            \"username\": \"Reiki\",\n" +
                "            \"title\": \"HR-M\"\n" +
                "        }\n" +
                "    ]\n" +
                "}";
        return  str;

    }

    public  static String  getCompanyInfo(){
        String str="{\n" +
                "    \"msg\": \"获取数据成功\",\n" +
                "    \"code\": 200,\n" +
                "    \"data\": [\n" +
                "        {\n" +
                "            \"id\": 1,\n" +
                "            \"logo\": \"https://www.itying.com/images/flutter/1.png\",\n" +
                "            \"name\": \"杭州蚂蚁金服信息技术有限公司  \",\n" +
                "            \"location\": \"上海新浦东区\",\n" +
                "            \"type\": \"互联网\",\n" +
                "            \"size\": \"B论\",\n" +
                "            \"employee\": \"1000人以上\",\n" +
                "            \"hot\": \"资深开放产品技术工程师\",\n" +
                "            \"count\": \"500\",\n" +
                "            \"inc\": \"蚂蚁金融服务集团(以下称\\\"蚂蚁金服\\\")起步于2004年成立的支付宝2014年10月\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"id\": 2,\n" +
                "            \"logo\": \"https://www.itying.com/images/flutter/2.png\",\n" +
                "            \"name\": \"百度信息技术有限公司  \",\n" +
                "            \"location\": \"广州天河区\",\n" +
                "            \"type\": \"互联网\",\n" +
                "            \"size\": \"C论\",\n" +
                "            \"employee\": \"500人以上\",\n" +
                "            \"hot\": \"全栈工程师\",\n" +
                "            \"count\": \"1000\",\n" +
                "            \"inc\": \"蚂蚁金融服务集团(以下称\\\"蚂蚁金服\\\")起步于2004年成立的支付宝2014年10月\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"id\": 3,\n" +
                "            \"logo\": \"https://www.itying.com/images/flutter/3.png\",\n" +
                "            \"name\": \"腾讯科有限公司  \",\n" +
                "            \"location\": \"深圳南山区\",\n" +
                "            \"type\": \"互联网\",\n" +
                "            \"size\": \"D论\",\n" +
                "            \"employee\": \"200人以上\",\n" +
                "            \"hot\": \"数据挖掘工程师\",\n" +
                "            \"count\": \"200\",\n" +
                "            \"inc\": \"蚂蚁金融服务集团(以下称\\\"蚂蚁金服\\\")起步于2004年成立的支付宝2014年10月\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"id\": 4,\n" +
                "            \"logo\": \"https://www.itying.com/images/flutter/4.png\",\n" +
                "            \"name\": \"字节跳动科技有限公司\",\n" +
                "            \"location\": \"北京海淀区\",\n" +
                "            \"type\": \"互联网\",\n" +
                "            \"size\": \"D论\",\n" +
                "            \"employee\": \"1500人以上\",\n" +
                "            \"hot\": \"资深架构师\",\n" +
                "            \"count\": \"1500\",\n" +
                "            \"inc\": \"蚂蚁金融服务集团(以下称\\\"蚂蚁金服\\\")起步于2004年成立的支付宝2014年10月\"\n" +
                "        }\n" +
                "    ]\n" +
                "}";
        return  str;
    }

    public  static  String  getMessageinfo(){
        String str="{\n" +
                "    \"msg\": \"获取数据成功\",\n" +
                "    \"code\": 200,\n" +
                "    \"data\": [\n" +
                "        {\n" +
                "            \"id\": 1,\n" +
                "            \"msg\": \"你好\",\n" +
                "            \"jobname\": \"HR\",\n" +
                "            \"companyname\": \"百度\",\n" +
                "            \"nickname\": \"小可爱\",\n" +
                "            \"headportraiturl\": \"https://www.itying.com/images/flutter/1.png\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"id\": 2,\n" +
                "            \"msg\": \"在吗\",\n" +
                "            \"jobname\": \"人事主管\",\n" +
                "            \"companyname\": \"腾讯\",\n" +
                "            \"nickname\": \"漩涡鸣人\",\n" +
                "            \"headportraiturl\": \"https://www.itying.com/images/flutter/2.png\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"id\": 3,\n" +
                "            \"msg\": \"最近在找工作吗\",\n" +
                "            \"jobname\": \"人事经理\",\n" +
                "            \"companyname\": \"字节跳动\",\n" +
                "            \"nickname\": \"雨落之后\",\n" +
                "            \"headportraiturl\": \"https://www.itying.com/images/flutter/3.png\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"id\": 4,\n" +
                "            \"msg\": \"你好 在吗？\",\n" +
                "            \"jobname\": \"人事助理\",\n" +
                "            \"companyname\": \"37互娱\",\n" +
                "            \"nickname\": \"马小玲\",\n" +
                "            \"headportraiturl\": \"https://www.itying.com/images/flutter/4.png\"\n" +
                "        }\n" +
                "    ]\n" +
                "}";

        return  str;

    }

}
